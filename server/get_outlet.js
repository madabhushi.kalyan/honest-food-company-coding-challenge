const superagent = require("superagent");

const KEY = "98b82d18fec444e0bb613a4fc3e1ece2";

const getLookup = require("./get_geojson").getLookup;

module.exports = (req, res, next) => {
  const { address } = req.query;

  superagent
    .get(`api.opencagedata.com/geocode/v1/json?q=${address}&key=${KEY}`)
    .then((response) => {
      const pos = response.body.results[0].geometry;
      const outlet = getLookup().search(pos.lng.toFixed(2), pos.lat.toFixed(2));

      res.status(200).json({
        ...outlet,
        user_location: { lat: pos.lat.toFixed(2), lng: pos.lng.toFixed(2) },
      });
    })
    .catch((error) => {
      next(error);
    });
};
