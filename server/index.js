const express = require("express");

const getOutlet = require("./get_outlet.js");

const app = express();

app.get("/ping", function (req, res) {
  return res.send("pong");
});

app.get("/api/get_outlet", getOutlet);

app.listen(5000, () => {
  console.log("Server running on port 5000");
});
