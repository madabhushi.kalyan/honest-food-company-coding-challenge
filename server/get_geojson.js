const path = require("path");
const tj = require("@mapbox/togeojson");
const fs = require("fs");
const DOMParser = require("xmldom").DOMParser;

const PolygonLookup = require("polygon-lookup");

let cachedLookup = {};

function getLookup() {
  if (Object.keys(cachedLookup).length > 0) {
    // If computed, return from cache. Dont recompute.
    return cachedLookup;
  }

  const kml = new DOMParser().parseFromString(
    fs.readFileSync(
      path.join("server", "resources", "delivery_areas.kml"),
      "utf8"
    )
  );

  const geoJson = tj.kml(kml);

  const lookup = new PolygonLookup(geoJson);

  // Set it into the cache, so on subsequent requests, u can fetch it
  // from cache rather than recomputing.
  cachedLookup = lookup;

  return lookup;
}

module.exports = {
  getLookup,
};
