import * as React from "react";
import { Map, Marker, TileLayer } from "react-leaflet";

export default function OutletMap({ lat, lng, zoom }) {
  // TODO: Add a polygon marker to show the boundary region
  // of the outlet to give a better visual indicator on
  // how the info can be shared
  return (
    <Map center={[lat, lng]} zoom={zoom}>
      <TileLayer
        url="https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png"
        attribution='&copy; <a href="http://osm.org/copyright">OpenStreetMap</a> contributors'
      />
      <Marker key={"marker"} position={[lat, lng]} />
    </Map>
  );
}
