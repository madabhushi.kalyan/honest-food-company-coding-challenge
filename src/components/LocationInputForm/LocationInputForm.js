import * as React from "react";
import { Input } from "antd";

import styles from "./location_input.module.css";

const LocationInputForm = ({ onSubmit }) => {
  // Function to remove accented char from the string as the geocoding api throws
  // an error on encountering any accented char.
  const removeAccents = (str) => {
    var accents =
      "ÀÁÂÃÄÅàáâãäåßÒÓÔÕÕÖØòóôõöøÈÉÊËèéêëðÇçÐÌÍÎÏìíîïÙÚÛÜùúûüÑñŠšŸÿýŽž";
    var accentsOut =
      "AAAAAAaaaaaaBOOOOOOOooooooEEEEeeeeeCcDIIIIiiiiUUUUuuuuNnSsYyyZz";
    str = str.split("");
    var strLen = str.length;
    var i, x;
    for (i = 0; i < strLen; i++) {
      if ((x = accents.indexOf(str[i])) != -1) {
        str[i] = accentsOut[x];
      }
    }
    return str.join("");
  };

  const handleSubmit = (event) => {
    event.preventDefault();
    onSubmit(removeAccents(address));
  };

  const [address, setAddress] = React.useState("");

  return (
    <form className={styles.locationwrapper} onSubmit={handleSubmit}>
      <div className={styles.address}>
        <Input onChange={(event) => setAddress(event.target.value)} />
      </div>

      <div>
        <button type="submit">Find outlet!</button>
      </div>
    </form>
  );
};

export default LocationInputForm;
