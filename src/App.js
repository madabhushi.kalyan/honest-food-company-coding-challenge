import React from "react";
import axios from "axios";

import "./App.css";
import LocationInputForm from "./components/LocationInputForm/LocationInputForm";
import OutletMap from "./components/Map/Map";
import { Card } from "antd";

function App() {
  const [outletData, setOutletData] = React.useState({});

  const fetchOutlet = (address) => {
    axios
      .get("/api/get_outlet", {
        params: { address },
      })
      .then((res) => {
        console.log("res", res.data);
        setOutletData(res.data);
      })
      .catch((err) => console.error(err));
  };

  return (
    <div className="App">
      <div className="heading">
        <h1>Welcome to the outlet finder!</h1>
      </div>
      <LocationInputForm onSubmit={fetchOutlet} />
      <div>
        {Object.keys(outletData).length > 0 ? (
          <>
            <div className="map">
              <OutletMap
                lat={outletData.user_location.lat}
                lng={outletData.user_location.lng}
                zoom={12}
              />
            </div>

            <Card title="Outlet Information" className="outlet-card">
              {outletData.properties ? (
                <ul>
                  <li>
                    Nearest Outlet:{" "}
                    <strong>{outletData.properties.name}</strong>
                  </li>
                </ul>
              ) : (
                "No outlet available!"
              )}
            </Card>
          </>
        ) : (
          <div className="map">
            {/** Show map centered in GMT... */}
            <OutletMap lat={51} lng={0} zoom={5} />
          </div>
        )}
      </div>
    </div>
  );
}

export default App;
